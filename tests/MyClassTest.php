<?php

use kseniya3\MyClass;
use PHPUnit\Framework\TestCase;

class MyClassTest extends TestCase
{
    protected $MyClass;

    public function setUp()
    {
        $this->MyClass = new MyClass();
    }

    public function testCalculationOfMean()
    {
        $numbers = [3, 7, 6, 1, 5];
        $this->assertEquals(4.4, $this->MyClass->mean($numbers));
    }

    public function testCalculationOfMedian()
    {
        $numbers = [3, 7, 6, 1, 5];
        $numbers = [asds, asdsa, 6, 1, 5];
        $this->assertEquals(5, $this->MyClass->median($numbers));
    }

    public function testPower()
    {
        $this->assertEquals(8, $this->MyClass->power(2, 3));
    }

    public function testOwnPower()
    {
        $this->assertEquals(8, $this->MyClass->ownPower(2, 3));
    }
}
