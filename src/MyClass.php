<?php

namespace kseniya3;

class MyClass
{
    /**
     * Расчет среднего значения 
     * @param array $numbers массив чисел
     * @return float среднее значение
     */
    public function mean(array $numbers)
    {
        return array_sum($numbers) / count($numbers);
    }

    /**
     * Расчитываем среднее значение
     * @param array $numbers массив чисел
     * @return float среднее значение
     */
    public function median(array $numbers)
    {
        sort($numbers);
        $size = count($numbers);

        if ($size % 2) {
            return $numbers[$size / 2];
        } else {
            return $this->mean(
                array_slice($numbers, ($size / 2) - 1, 2)
            );
        }
    }

    public function power($x, $y)
    {
        return pow($x, $y);
    }

    public function ownPower($number, $n)
    {
        $sum = 1;
        for ($i = 0; $i < $n; $i++)
            $sum *= $number;
        return $sum;
    }
}
